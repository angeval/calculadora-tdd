package com.testtdd.calculadora;

import java.util.List;

public class Operacao {

    public static int soma(List<Integer> numeros){
        int resultado = 0;
        for (Integer numero: numeros) {
            resultado += numero;
        }
        return resultado;
    }

    public static int soma(int numero1, int numero2){
        int resultado = 0;
        resultado = numero1 + numero2;
        return resultado;
    }

    public static int subtrai(List<Integer> numeros){
        int resultado = 0;
        for (int numero:numeros){
            if (numeros.indexOf(numero) == 0){
                resultado = numero;
            }
            else{
                resultado -= numero;}
        }
        return resultado;
    }

    public static int subtrai(int numero1, int numero2){
        int resultado = 0;
        resultado = numero1 - numero2;
        return resultado;
    }

    public static int multiplica(List<Integer> numeros){
        int resultado = 1;
        for (int numero:numeros){
            resultado *= numero;
        }
        return resultado;
    }

    public static int multiplica(int numero1, int numero2){
        int resultado = 1;
        resultado = numero1 * numero2;
        return resultado;
    }

    public static int divide(List<Integer> numeros){
        int resultado = 0;
        int divisor = 0;
        int dividendo = 0;

        for (int numero:numeros){
            if (numero > dividendo){
                divisor = dividendo;
                dividendo = numero;
            }
            else{
                divisor = numero;
            }
        }
        resultado = dividendo / divisor;
        return resultado;
    }

    public static int divide(int numero1, int numero2){
        int resultado = 0;
        int divisor = 0;
        int dividendo = 0;

        if (numero1 > numero2){
            resultado = numero1 / numero2;
            }
        else{
            resultado = numero2 / numero1;
        }
        return resultado;
    }
}

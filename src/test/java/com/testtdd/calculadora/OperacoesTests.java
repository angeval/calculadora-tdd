package com.testtdd.calculadora;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class OperacoesTests {

    @Test
    public void testarOperacaoDeSoma(){
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(1);
        numeros.add(2);
        numeros.add(3);
        numeros.add(4);
        Assertions.assertEquals(Operacao.soma(numeros), 10);
    }

    @Test
    public void testarOperacaoDeSomaComDoisNumeros(){
        int numero1 =1;
        int numero2 =2;
        Assertions.assertEquals(Operacao.soma(numero1, numero2), 3);
    }

    @Test
    public void testarOperacaoDeSubtracao(){
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(4);
        numeros.add(2);
        numeros.add(1);
        Assertions.assertEquals(Operacao.subtrai(numeros), 1);
    }

    @Test
    public void testarOperacaoDeSubtracaoComDoisNumeros(){
        int numero1 =1;
        int numero2 =2;
        Assertions.assertEquals(Operacao.subtrai(numero1, numero2), -1);
    }

    @Test
    public void testarOperacaoDeMultiplicacao(){
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(4);
        numeros.add(2);
        numeros.add(1);
        Assertions.assertEquals(Operacao.multiplica(numeros), 8);
    }

    @Test
    public void testarOperacaoDeMultiplicacaoComDoisNumeros(){
        int numero1 =1;
        int numero2 =2;
        Assertions.assertEquals(Operacao.multiplica(numero1, numero2), 2);
    }

    @Test
    public void testarOperacaoDeMultiplicacaoComZero(){
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(4);
        numeros.add(2);
        numeros.add(0);
        Assertions.assertEquals(Operacao.multiplica(numeros), 0);
    }

    @Test
    public void testarOperacaoDeDivisaoComDoisNumeros(){
        int numero1 =2;
        int numero2 =4;
        Assertions.assertEquals(Operacao.divide(numero1, numero2), 2);
    }

    @Test
    public void testarOperacaoDeDivisao(){
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(4);
        numeros.add(2);
        Assertions.assertEquals(Operacao.divide(numeros), 2);
    }

    @Test
    public void testarOperacaoDeDivisaoComNumerosInvalidos(){
        List<Integer> numeros = new ArrayList<Integer>();
        numeros.add(4);
        numeros.add(0);
        Assertions.assertThrows(ArithmeticException.class, () -> {Operacao.divide(numeros);});
    }
}
